
/**
 * Created by cat on 9/19/18.
 * 
 * KEY: Test all functions in AronCLib.h
 * 
 * cd $b/cpp
 * gcc -I$b/cpplib -o  /tmp/t  AronCLibTest.c
 *
 */

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "AronCLib.h"


void fun(char arr[]){
    arr[0] = 'a';
    arr[1] = 'a';
}

struct Type{
    int num;
} Type;
typedef struct Type MyType;


int cmpInt(const void* p1, const void* p2){
    int left = *(int*)p1;
    int right = *(int*)p2;
    return left - right;
}

int cmpType(const void* p1, const void* p2){
    MyType* tp1 = (MyType*)p1;
    MyType* tp2 = (MyType*)p2;
    if(tp1 -> num < tp2 -> num)
        return -1;
    else if(tp1 -> num > tp2 -> num)
        return 1;
    else
        return 0;
}

TEST_CASE( "AronCLib.h") {
    {
        REQUIRE(true == true);
    }
    {
        // test removeDuplicate()
        char *str = "";
        char *newPtr = (char*)malloc((strlen(str) + 1)*sizeof(char));
        removeDuplicate(str, newPtr);
        char *expStr = "";
        REQUIRE(strlen(newPtr) == 0);
        REQUIRE(strcmp(newPtr, expStr) == 0);
        free(newPtr);
    }
    {
        // test removeDuplicate()
        char *str = "a";
        char *newPtr = (char*)malloc((strlen(str) + 1)*sizeof(char));
        removeDuplicate(str, newPtr);
        char *expStr = "a";
        REQUIRE(strlen(newPtr) == 1);
        REQUIRE(strcmp(newPtr, expStr) == 0);
        free(newPtr);
    }
    {
        // test removeDuplicate()
        char *str = "aba";
        char *newPtr = (char*)malloc((strlen(str) + 1)*sizeof(char));
        removeDuplicate(str, newPtr);
        char *expStr = "ab";
        REQUIRE(strlen(newPtr) == 2);
        REQUIRE(strcmp(newPtr, expStr) == 0);
        free(newPtr);
    }
    {
        // test removeDuplicate()
        char *str = "aaaa";
        char *newPtr = (char*)malloc((strlen(str) + 1)*sizeof(char));
        removeDuplicate(str, newPtr);
        char *expStr = "a";
        REQUIRE(strlen(newPtr) == 1);
        REQUIRE(strcmp(newPtr, expStr) == 0);
        free(newPtr);
    }
    {
        // test removeDuplicate()
        char *str = "aaaabba";
        char *newPtr = (char*)malloc((strlen(str) + 1)*sizeof(char));
        removeDuplicate(str, newPtr);
        char *expStr = "ab";
        REQUIRE(strlen(newPtr) == 2);
        REQUIRE(strcmp(newPtr, expStr) == 0);
        free(newPtr);
    }

    {
        {
            // 0, 1, 2
            const int NUM = 3;
            MyType** arr = (MyType**)malloc(sizeof(MyType*)*NUM);
            for(int i=0; i<NUM; i++){
                arr[i] = (MyType*) malloc(sizeof(MyType)*1);
                arr[i] -> num = i;
            }
            
            int lo = 0;
            int hi = 2;
            int pivot = partitionAny((void**)arr, cmpType, lo, hi);
            REQUIRE(pivot == 2);
            
            
            for(int i=0; i<NUM; i++){
                if(arr[i])
                    delete arr[i];
            }

            {
                char arr[] = "a";
                int num = len(arr);
                const int NUM = 26;
                uniqueOrder(arr, &num, NUM);
                printf("num=%d\n", num);
                printArray(arr, num);
                
                char expArr[] = "a";
                int  expnum = 1;
                REQUIRE(num == expnum);
                REQUIRE(arr[0] == expArr[0]);
                
            }
            {
                char arr[] = "abab";
                int num = len(arr);
                const int NUM = 26;
                uniqueOrder(arr, &num, NUM);
                printf("num=%d\n", num);
                printArray(arr, num);
                
                char expArr[] = "ab";
                int  expnum = 2;
                REQUIRE(num == expnum);
                REQUIRE(compareArray(arr, len(arr), expArr, len(expArr)) == true);
            }
            {
                char arr[] = "";
                int num = len(arr);
                const int NUM = 26;
                uniqueOrder(arr, &num, NUM);
                printf("num=%d\n", num);
                printArray(arr, num);
                
                char expArr[] = "";
                int  expnum = 0;
                REQUIRE(num == expnum);
                REQUIRE(compareArray(arr, len(arr), expArr, len(expArr)) == true);
            }
            {
                char arr[] = "abcceeaakkkkkkkkkkkkkkkkkkkkkkk";
                int num = len(arr);
                const int NUM = 26;
                uniqueOrder(arr, &num, NUM);
                printf("num=%d\n", num);
                printArray(arr, num);
                
                char expArr[] = "abcek";
                int  expnum = 5;
                REQUIRE(num == expnum);
                REQUIRE(compareArray(arr, len(arr), expArr, len(expArr)) == true);
            }
            {
                {
                    int n = 0;
                    int num = factorial(n);
                    REQUIRE(num == 1);
                }
                {
                    int n = 1;
                    int num = factorial(n);
                    REQUIRE(num == 1);
                }
                {
                    int n = 2;
                    int num = factorial(n);
                    REQUIRE(num == 2);
                }
                {
                    int n = 3;
                    int num = factorial(n);
                    REQUIRE(num == 6);
                }
            }
            {

                {
                    char arr[] = "";
                    removeIndexChar(arr, 0);
                    REQUIRE(strcmp(arr, "") == 0);
                }
                {
                    char arr[] = "a";
                    removeIndexChar(arr, 0);
                    REQUIRE(strcmp(arr, "") == 0);
                }
                {
                    char arr[] = "ab";
                    removeIndexChar(arr, 0);
                    REQUIRE(strcmp(arr, "b") == 0);
                }
                {
                    char arr[] = "ab";
                    removeIndexChar(arr, 1);
                    REQUIRE(strcmp(arr, "a") == 0);
                }
                {
                    char arr[] = "abc";
                    removeIndexChar(arr, 1);
                    REQUIRE(strcmp(arr, "ac") == 0);
                }
            }
            {
                {
                    char arr[] = "a";
                    dropChar(1, arr);
                    REQUIRE(strcmp(arr, "") == 0);
                }
                {
                    char arr[] = "a";
                    dropChar(0, arr);
                    REQUIRE(strcmp(arr, "a") == 0);
                }
                {
                    char arr[] = "";
                    dropChar(1, arr);
                    REQUIRE(strcmp(arr, "") == 0);
                }
                {
                    char arr[] = "abc";
                    dropChar(1, arr);
                    REQUIRE(strcmp(arr, "bc") == 0);
                }
                {
                    char arr[] = "abc";
                    dropChar(3, arr);
                    REQUIRE(strcmp(arr, "") == 0);
                }
                {
                    char arr[] = "abcde";
                    dropChar(3, arr);
                    REQUIRE(strcmp(arr, "de") == 0);
                }
            }
            {
                {
                    char arr[] = "abcde";
                    char narr[6];
                    takeChar(arr, 3, narr);
                    REQUIRE(strcmp(narr, "abc") == 0);
                }
                {
                    char arr[] = "abcde";
                    char narr[6];
                    takeChar(arr, 0, narr);
                    REQUIRE(strcmp(narr, "") == 0);
                }
                {
                    char arr[] = "abcde";
                    char narr[6];
                    takeChar(arr, 5, narr);
                    REQUIRE(strcmp(narr, "abcde") == 0);
                }
                {
                    char* pt = (char*)malloc(sizeof(char)*10);
                    free(pt);
                }
            }
            {
                {
                    char arr1[] = "abc";
                    char arr2[] = "def";
                    char arr3[7];
                    // arr3 = arr1 + arr2
                    concat(arr1, arr2, arr3);
                    printf("%s\n", arr3);
                    REQUIRE(strcmp(arr3, "abcdef") == 0);
                }
                {
                    char arr1[] = "a";
                    char arr2[] = "b";
                    char arr3[7];
                    concat(arr1, arr2, arr3);
                    REQUIRE(strcmp(arr3, "ab") == 0);
                }
                {
                    char arr1[] = "";
                    char arr2[] = "";
                    char arr3[7];
                    concat(arr1, arr2, arr3);
                    REQUIRE(strcmp(arr3, "") == 0);
                }
                {
                    char arr1[] = "";
                    char arr2[] = "b";
                    char arr3[3];
                    concat(arr1, arr2, arr3);
                    REQUIRE(strcmp(arr3, "b") == 0);
                }
                {
                    char arr1[] = "a";
                    char arr2[] = "";
                    char arr3[3];
                    concat(arr1, arr2, arr3);
                    REQUIRE(strcmp(arr3, "a") == 0);
                }
            }
        }
        {
            // test shiftRightChar
            {
                char arr[] = "ab";
                shiftRightChar(arr, 0);
                printf("arr=%s\n", arr);
                REQUIRE(isEqualStr(arr, "ab"));
            }
            {
                char arr[] = "";
                shiftRightChar(arr, 0);
                REQUIRE(isEqualStr(arr, ""));
            }
            {
                char arr[] = "a";
                shiftRightChar(arr, 1);
                REQUIRE(isEqualStr(arr, "a"));
            }
            {
                char arr[] = "abc";
                shiftRightChar(arr, 0);
                REQUIRE(isEqualStr(arr, "abc"));
            }
            {
                char arr[] = "abcd";
                shiftRightChar(arr, 1);
                REQUIRE(isEqualStr(arr, "dabc"));
            }
            {
                char arr[] = "abcd";
                shiftRightChar(arr, 2);
                REQUIRE(isEqualStr(arr, "cdab"));
            }
            {
                char arr[] = "abcd";
                shiftRightChar(arr, 3);
                REQUIRE(isEqualStr(arr, "bcda"));
            }
            {
                char arr[] = "abcd";
                shiftRightChar(arr, 4);
                REQUIRE(isEqualStr(arr, "abcd"));
            }
        }
        {
            // test shiftLeftChar
            {
                char arr[] = "ab";
                shiftLeftChar(arr, 0);
                printf("arr=%s\n", arr);
                REQUIRE(isEqualStr(arr, "ab"));
            }
            {
                char arr[] = "";
                shiftLeftChar(arr, 0);
                REQUIRE(isEqualStr(arr, ""));
            }
            {
                char arr[] = "a";
                shiftLeftChar(arr, 1);
                REQUIRE(isEqualStr(arr, "a"));
            }
            {
                char arr[] = "abc";
                shiftLeftChar(arr, 0);
                REQUIRE(isEqualStr(arr, "abc"));
            }
            {
                char arr[] = "abcd";
                shiftLeftChar(arr, 1);
                REQUIRE(isEqualStr(arr, "bcda"));
            }
            {
                char arr[] = "abcd";
                shiftLeftChar(arr, 2);
                REQUIRE(isEqualStr(arr, "cdab"));
            }
            {
                char arr[] = "abcd";
                shiftLeftChar(arr, 3);
                REQUIRE(isEqualStr(arr, "dabc"));
            }
            {
                char arr[] = "abcd";
                shiftLeftChar(arr, 4);
                REQUIRE(isEqualStr(arr, "abcd"));
            }
            {
            }
        }
    }
 
}
